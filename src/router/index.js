import Vue from 'vue'
import Router from 'vue-router'
import Axios1 from '@/components/RainAxios'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Axios',
      component: Axios1
    }
  ]
})
